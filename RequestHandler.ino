#include <WiFi.h>

const char* ssid     = "Q's AP, full of IOT";
const char* password = "natalitsa";
//kalhspera

//request data
String requestMethod = "";  //self-explanatory
String requestPath = "";    //self-explanatory
String requestVersion = ""; //self-explanatory
String headers = "";
String values = "";
String requestBody = "";


WiFiServer server(80);
WiFiClient client;

//Static IP
IPAddress local_IP(192, 168, 1, 5);
IPAddress gateway(192, 168, 2, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress primaryDNS(8, 8, 8, 8); //optional
IPAddress secondaryDNS(8, 8, 4, 4); //optional
bool useStaticIP = true;

void receiveRequest();
void response200();
void printHTML();

void setup()
{
    Serial.begin(115200);
    while(!Serial){ delay (50); }

    if(useStaticIP)
      if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
        Serial.println("STA Failed to configure");
      }


    Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected.");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());

    server.begin();

}


void loop(){
 client = server.available(); // listen for incoming clients
  if (client) {               // if you get a client,
    Serial.println();
    Serial.println("New Client.");
    receiveRequest();
  }
}

void receiveRequest(){
    uint8_t wordPointer = 0;  //counts the word number of a line, increase if c==" "
    uint8_t linePointer = 255;  //counts the number of a line, increase if c=="\n", -1 for Request-Line

    bool textIsBody = false;  //flag turns true when the body data begins

    String currentLine = "";  // make a String to hold incoming data from the client
    char c; //new character

  while (client.connected()) { // loop while the client's connected and

    if (client.available()) {  // if there's bytes to read from the client,

      char c = client.read();  //read the new byte and
      Serial.write(c);  // print it out the serial monitor

      if (textIsBody){
        Serial.println("Writing at Body!");
        requestBody += c;
      }
      else{

      if (c == ' ')  wordPointer++;
      if (c == '\n'){
        values += '\n';
        headers += '\n';
        linePointer++; wordPointer=0;
      }


      if (linePointer == 255 && c != ' '){  //runs only for the first line in order to register Method/Path/Version (word 0/1/2)

        if (wordPointer == 0)
          requestMethod += c;
        else if (wordPointer == 1)
          requestPath += c;
        else
          requestVersion += c;

      }  //End of: if (linePointer == -1)

      if (c == '\n') {  //if the byte is a newline character

        if (currentLine.length() == 0) {
          //----------HERE LIES THE END OF REQUEST HEADERS-------------//
          endOfHeader();
          textIsBody=true;
        } //End of: if (currentLine.length() == 0)
        else { // if you got a newline, then clear currentLine:
            currentLine = "";
        } //End of else

      } //if (c == '\n')
      else if (c != '\r') { // if you got anything else but a carriage return character,
        currentLine += c; // add it to the end of the currentLine
        if(wordPointer == 0)
          headers += c;
        else
          values +=c;
      }
      } //End of else (!textIsBody)
    } //End of: if (client.available())
  } //End of: while (client.connected())

  // close the connection:
  client.stop();
  Serial.println("Client Disconnected.");
  Serial.println();
  Serial.println(requestMethod);
  Serial.println(requestPath);
  Serial.println(requestVersion);
  Serial.println();
  Serial.println(headers);
  Serial.println();
  Serial.println(values);
  Serial.println();
  Serial.println(requestBody);
}

void endOfHeader(){

  if(requestMethod == "GET"){
    Serial.println("End of Request Headers");
    response200();  //TESTING !!!
    if(requestPath == "/test")
      printHTML();
  }
  client.stop();  //TESTING !!!
}

void response200(){
  // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
  // and a content-type so the client knows what's coming, then a blank line:
  client.println("HTTP/1.1 200 OK");
  client.println("Content-type: text/html");
  //client.println("Content-length: 100");
  client.println("Connection: close");
  client.println();
}

void printHTML(){
  Serial.println("Sending HTML");
  client.println("<!DOCTYPE HTML><html>");  //Start of the HTML file

  //Start of Head
  client.println("<head>");
    client.println("<title>Placeholder</title>");  //Enter title
  client.println("</head>");  //End of Head

  //Start of Body
  client.println("<body>");
    client.println("<p> OK </p>");
  client.println("</body>");  //End of body
  client.println("</html>");  //End of the HTML file
}

/*void oldLoop(){
  if (client) {                             // if you get a client,
    Serial.println();
    Serial.println("New Client.");          // print a message out the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        if (c == '\n') {                    // if the byte is a newline character

          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println();

            // the content of the HTTP response follows the header:
            client.print("Click <a href=\"/H\">here</a> to turn the LED on pin 5 on.<br>");
            client.print("Click <a href=\"/L\">here</a> to turn the LED on pin 5 off.<br>");

            // The HTTP response ends with another blank line:
            client.println();
            // break out of the while loop:
            break;
          } else {    // if you got a newline, then clear currentLine:
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }

        // Check to see if the client request was "GET /H" or "GET /L":
        if (currentLine.endsWith("GET /H")) {
          digitalWrite(5, HIGH);               // GET /H turns the LED on
        }
        if (currentLine.endsWith("GET /L")) {
          digitalWrite(5, LOW);                // GET /L turns the LED off
        }
      }
    }
    // close the connection:
    client.stop();
    Serial.println("Client Disconnected.");
  }
} */

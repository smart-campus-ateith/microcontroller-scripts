#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WebServer.h>


// Object for storin multiple APs' credentials
ESP8266WiFiMulti wifiMulti;

// Status of WiFi connection
bool connected = false;

// Object for server, more info at
// https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WebServer
ESP8266WebServer server(80);

void handleBody();
void setup() {
  // Begin Serial Communication
  Serial.begin(115200);

  // WiFi set as station aka client
  WiFi.mode(WIFI_STA);

  // Add your known networks
  wifiMulti.addAP("ssid_from_AP_2", "your_password_for_AP_1");
  wifiMulti.addAP("ssid_from_AP_2", "your_password_for_AP_2");
  wifiMulti.addAP("ssid_from_AP_3", "your_password_for_AP_3");

  // Attach routes to functions,
  // you know, like evey other freaking backend framework
  server.on("/body", handleBody);

  // Start the server, obviously
  server.begin();

}

void loop() {
  // if the WiFi is connected
  if (wifiMulti.run() == WL_CONNECTED) {
    if (!connected) { //and was not connected previously
      connected = true; // update its status
      Serial.println("Connection established!"); // notify the user
      Serial.print("IP address:\t"); // print the new IP address
      Serial.println(WiFi.localIP());
    }
//CODE HERE


server.handleClient(); //Handling of incoming requests



  }else{ // WiFi not connected
    connected = false; // update its status
    if (!connected)
      Serial.println("WiFi Disconnected!");
    delay(1000);
  }

}

void handleBody() { //Handler for the body path
  if (server.hasArg("plain")== false){ //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }

  String message = "Body received:\n";
         message += server.arg("plain");
         message += "\n";

  server.send(200, "text/plain", message);
  Serial.println(message);
}
